package api.test;

import com.google.api.server.spi.config.Api;

import api.constants.Constants;
import api.model.EndpointResponse;

@Api(name = "testAPI", version = "v1",
	 scopes = { Constants.EMAIL_SCOPE }, 
	 clientIds = { Constants.WEB_CLIENT_ID }, 
	 audiences = { Constants.WEB_CLIENT_ID })
public class TestAPI {

	public EndpointResponse testApi(){
		EndpointResponse resp = new EndpointResponse();
		resp.setStatus(200);
		resp.setMessage("Voila. It works!!!!!!!!!");
		return resp;
	}
	
}
