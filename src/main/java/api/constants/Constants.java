package api.constants;

/**
 * Contains the client IDs and scopes for allowed clients consuming the helloworld API.
 */
public class Constants {
  public static final String WEB_CLIENT_ID = "1086497618382-t6bnjgn2j9gi5a036b4dmj73hg4sobdc.apps.googleusercontent.com";

  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}
